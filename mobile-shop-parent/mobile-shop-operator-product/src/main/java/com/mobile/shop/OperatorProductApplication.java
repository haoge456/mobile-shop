package com.mobile.shop;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = "com.mobile.shop.mapper")
public class OperatorProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(OperatorProductApplication.class,args);
    }


}
