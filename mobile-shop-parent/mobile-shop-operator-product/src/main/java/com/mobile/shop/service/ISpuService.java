package com.mobile.shop.service;

import com.mobile.shop.entity.Spu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author QHM
 * @since 2020-06-02
 */
public interface ISpuService extends IService<Spu> {

}
