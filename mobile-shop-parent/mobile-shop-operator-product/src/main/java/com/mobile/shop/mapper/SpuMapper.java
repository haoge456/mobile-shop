package com.mobile.shop.mapper;

import com.mobile.shop.entity.Spu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author QHM
 * @since 2020-06-02
 */
public interface SpuMapper extends BaseMapper<Spu> {

}
