package com.mobile.shop.service;

import com.mobile.shop.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author QHM
 * @since 2020-06-05
 */
public interface ICommentService extends IService<Comment> {

}
