package com.mobile.shop.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.entity.Comment;
import com.mobile.shop.entity.ResultEntity;
import com.mobile.shop.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author QHM
 * @since 2020-06-05
 */
@RestController
@RequestMapping("/comment/")
public class CommentController {


    @Autowired
    ICommentService iCommentService;

   @RequestMapping("findAll")
   public ResultEntity findAll(@RequestParam(defaultValue = "1") Integer current,
                               @RequestParam(defaultValue = "3") Integer size,Comment comment){

       QueryWrapper queryWrapper=new QueryWrapper<>();

       if (comment.getId()!=null) {

           queryWrapper.eq("id",comment.getId());

       }
       if (comment.getUid()!=null) {

           queryWrapper.like("uid",comment.getUid());

       }

       Page page = new Page(current, size);

       if (iCommentService.page(page, queryWrapper) != null) {

           return ResultEntity.ok(iCommentService.page(page, queryWrapper));

       } else {

           return ResultEntity.error(iCommentService.page(page, queryWrapper));
       }



   }
    @RequestMapping("dele")
    public void dele(String id ){

        iCommentService.removeById(id);

    }

}

