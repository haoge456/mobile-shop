package com.mobile.shop.service.impl;

import com.mobile.shop.entity.Spu;
import com.mobile.shop.mapper.SpuMapper;
import com.mobile.shop.service.ISpuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author QHM
 * @since 2020-05-25
 */
@Service
public class SpuServiceImpl extends ServiceImpl<SpuMapper, Spu> implements ISpuService {

}
