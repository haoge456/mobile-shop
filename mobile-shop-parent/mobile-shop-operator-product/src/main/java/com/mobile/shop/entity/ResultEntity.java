package com.mobile.shop.entity;

import java.io.Serializable;

public class ResultEntity implements Serializable {

    private static final long serialVersionUID=1L;

    //返回状态码
    private Integer code;
    //返回装填吗对应文本信息；
    private String msg;
    //返回的数据
    private Object data;

    public ResultEntity(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Object getData() {
        return data;
    }


    @Override
    public String toString() {
        return "ResultEntity{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
//ok静态方法统一返回的数据格式
    public static ResultEntity ok(Object data){
        return new ResultEntity(200,"ok",data);
    }
    //error  错误同一
    public static ResultEntity error(Object data){
        return new ResultEntity(400,"error",data);
    }



}
