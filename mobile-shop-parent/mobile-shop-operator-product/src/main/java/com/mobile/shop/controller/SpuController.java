package com.mobile.shop.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mobile.shop.entity.ResultEntity;
import com.mobile.shop.entity.Spu;
import com.mobile.shop.service.ISpuService;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author QHM
 * @since 2020-06-02
 */
@RestController
@RequestMapping("/spu/")
public class SpuController {
    @Resource
    ISpuService iSpuService;

    @RequestMapping("findAll")
    public ResultEntity findAll(@RequestParam(defaultValue = "1") Integer current,
                                @RequestParam(defaultValue = "3") Integer size, Spu spu ){

        QueryWrapper queryWrapper=new QueryWrapper<>();

        if (spu.getId()!=null) {

            queryWrapper.eq("id",spu.getId());

        }
        if (spu.getName()!=null&&spu.getName()!="") {

            queryWrapper.like("name",spu.getName());

        }

        Page page = new Page(current, size);

        if (iSpuService.page(page, queryWrapper) != null) {

            return ResultEntity.ok(iSpuService.page(page, queryWrapper));

        } else {

            return ResultEntity.error(iSpuService.page(page, queryWrapper));
        }

    }
    @PostMapping("addspu")
    public ResultEntity addspu(@RequestBody Spu spu){

        return ResultEntity.ok(iSpuService.save(spu));

    }

    @PostMapping("updatespu")
    public ResultEntity updatespu(@RequestBody Spu spu){

        return ResultEntity.ok(iSpuService.updateById(spu));

    }


    @RequestMapping("selebyId")
    public ResultEntity selebyId(String id){

        return ResultEntity.ok(iSpuService.getById(id));

    }
    @RequestMapping("dele")
    public void dele(String id ){

        iSpuService.removeById(id);

    }
    @RequestMapping("/upload")
    public ResultEntity upload(MultipartFile file){
        try {
            //判断上传的文件了
            if (file != null && !file.isEmpty()) {
                //上传路径地址
                String path = "D:\\pic\\";
                //重新命名文件的名称
                String fileName = UUID.randomUUID() + "_" + file.getOriginalFilename();
                //创建文件对象
                File destFile = new File(path, fileName);
                //当前文件进行拷贝
                file.transferTo(destFile);
                //返回图片的路径地址
                return ResultEntity.ok("http://localhost:8388/img/"+fileName);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return ResultEntity.error("upload");
    }


}

