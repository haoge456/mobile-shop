package com.mobile.shop.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author QHM
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("jy_spu")
public class Spu implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品图片
     */
    private String pic;

    /**
     * 是否是新品
     */
    @TableField("isNew")
    private Integer isNew;

    /**
     * 是否热卖
     */
    @TableField("isHot")
    private Integer isHot;

    /**
     * 商品介绍
     */
    private String products;

    /**
     * 栏目id
     */
    private Integer cid;

    /**
     * 品牌id
     */
    private Integer bid;


}
