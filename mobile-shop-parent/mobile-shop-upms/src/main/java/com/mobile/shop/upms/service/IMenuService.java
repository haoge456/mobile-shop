package com.mobile.shop.upms.service;

import com.mobile.shop.upms.entity.Menu;
import com.baomidou.mybatisplus.service.IService;
import com.mobile.shop.upms.vo.MenuVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface IMenuService extends IService<Menu> {

    List<MenuVo> getMenuByUserName(String name);

}
