package com.mobile.shop.upms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: MobileShopUpmsApplications <br/>
 * Description: <br/>
 * date: 2020/6/2 20:54<br/>
 *
 * @author lenovo<br />
 * @version v1.0
 * @since JDK 1.8
 */
@SpringBootApplication
@MapperScan("com.mobile.shop.upms.mapper")
public class MobileShopUpmsApplications {
    public static void main(String[] args) {
        SpringApplication.run(MobileShopUpmsApplications.class,args);
    }
}
