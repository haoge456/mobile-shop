package com.mobile.shop.upms.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@RestController
@RequestMapping("/role")
public class RoleController {

}
