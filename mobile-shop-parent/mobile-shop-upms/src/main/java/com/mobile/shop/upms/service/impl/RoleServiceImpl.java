package com.mobile.shop.upms.service.impl;

import com.mobile.shop.upms.entity.Role;
import com.mobile.shop.upms.mapper.RoleMapper;
import com.mobile.shop.upms.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
