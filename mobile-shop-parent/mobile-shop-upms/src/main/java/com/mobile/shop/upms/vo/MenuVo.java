package com.mobile.shop.upms.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.mobile.shop.upms.entity.Menu;
import lombok.Data;

import java.util.List;

/**
 * ClassName: MenuVo <br/>
 * Description: <br/>
 * date: 2020/6/2 21:03<br/>
 *
 * @author lenovo<br />
 * @version v1.0
 * @since JDK 1.8
 */
@Data
public class MenuVo extends Menu{

    @TableField(exist = false)
    private List<Menu> children;
}
