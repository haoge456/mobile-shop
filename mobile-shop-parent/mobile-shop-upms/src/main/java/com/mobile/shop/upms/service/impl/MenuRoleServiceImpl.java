package com.mobile.shop.upms.service.impl;

import com.mobile.shop.upms.entity.MenuRole;
import com.mobile.shop.upms.mapper.MenuRoleMapper;
import com.mobile.shop.upms.service.IMenuRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class MenuRoleServiceImpl extends ServiceImpl<MenuRoleMapper, MenuRole> implements IMenuRoleService {

}
