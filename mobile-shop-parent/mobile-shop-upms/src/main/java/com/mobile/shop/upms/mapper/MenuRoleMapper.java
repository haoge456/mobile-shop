package com.mobile.shop.upms.mapper;

import com.mobile.shop.upms.entity.MenuRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface MenuRoleMapper extends BaseMapper<MenuRole> {

}
