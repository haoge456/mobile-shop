package com.mobile.shop.upms.web;


import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.upms.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    IMenuService iMenuService;


    @GetMapping("getMenuByUsername")
    public ResultEntity getMenuByUsername(String name){

        return ResultEntity.ok(iMenuService.getMenuByUserName(name));
    }

}
