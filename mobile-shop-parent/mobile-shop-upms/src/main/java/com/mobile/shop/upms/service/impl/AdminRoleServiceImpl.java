package com.mobile.shop.upms.service.impl;

import com.mobile.shop.upms.entity.AdminRole;
import com.mobile.shop.upms.mapper.AdminRoleMapper;
import com.mobile.shop.upms.service.IAdminRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole> implements IAdminRoleService {

}
