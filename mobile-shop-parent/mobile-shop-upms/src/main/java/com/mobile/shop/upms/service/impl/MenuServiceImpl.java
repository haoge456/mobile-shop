package com.mobile.shop.upms.service.impl;

import com.mobile.shop.upms.entity.Menu;
import com.mobile.shop.upms.mapper.MenuMapper;
import com.mobile.shop.upms.service.IMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mobile.shop.upms.vo.MenuVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<MenuVo> getMenuByUserName(String name) {
        return menuMapper.getMenuByUserName(name);
    }
}
