package com.mobile.shop.upms.service;

import com.mobile.shop.upms.entity.AdminRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface IAdminRoleService extends IService<AdminRole> {

}
