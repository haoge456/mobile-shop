package com.mobile.shop.upms.mapper;

import com.mobile.shop.upms.entity.Menu;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mobile.shop.upms.vo.MenuVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<MenuVo> getMenuByUserName(String name);
}
