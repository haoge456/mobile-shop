package com.mobile.shop.upms.service.impl;

import com.mobile.shop.upms.entity.Admin;
import com.mobile.shop.upms.mapper.AdminMapper;
import com.mobile.shop.upms.service.IAdminService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

}
