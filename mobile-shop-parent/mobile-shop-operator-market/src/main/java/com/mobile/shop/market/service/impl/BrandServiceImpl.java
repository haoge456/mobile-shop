package com.mobile.shop.market.service.impl;

import com.mobile.shop.market.entity.Brand;
import com.mobile.shop.market.mapper.BrandMapper;
import com.mobile.shop.market.service.IBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {

}
