package com.mobile.shop.market.service.impl;

import com.mobile.shop.market.entity.Order;
import com.mobile.shop.market.entity.OrderDetailSku;
import com.mobile.shop.market.mapper.OrderMapper;
import com.mobile.shop.market.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private  OrderMapper orderMapper;
    @Override
    public List<OrderDetailSku> selectDetail(Integer id) {
        return orderMapper.selectDetail(id);
    }

    @Override
    public String selectAddress(Integer id) {
        return orderMapper.selectAddress(id);
    }
}
