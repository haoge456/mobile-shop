package com.mobile.shop.market.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.market.entity.Brand;
import com.mobile.shop.market.service.IBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private IBrandService iBrandService;

    @RequestMapping("/list")
    public ResultEntity list(@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,@RequestParam(value = "pageSize",defaultValue = "3")Integer pageSize,
                      Brand brand
                      ){

        Page<Brand> page = new Page<>(pageNum, pageSize);

        QueryWrapper<Brand> wrapper = new QueryWrapper<>();
        //对品牌商进行模糊查询
        if(brand.getName()!=null)
        wrapper.like("name",brand.getName());

        return  ResultEntity.ok(iBrandService.page(page,wrapper));
    }


    @PostMapping("/insert")
    public ResultEntity insert(@RequestBody Brand brand
    ){

        boolean b = iBrandService.save(brand);
        if(b){
            return  ResultEntity.ok("添加品牌成功",200);
        }


        return  ResultEntity.error("添加品牌失败",500);
    }

    @PostMapping("/update")
    public ResultEntity update(@RequestBody Brand brand
    ){

        boolean b = iBrandService.updateById(brand);
        if(b){
            return  ResultEntity.ok("修改品牌成功",200);
        }


        return  ResultEntity.error("修改品牌失败",500);
    }

    @RequestMapping("/delete")
    public ResultEntity delete(Brand brand
    ){
        boolean b = iBrandService.removeById(brand.getId());
        if(b){
            return  ResultEntity.ok("删除品牌成功",200);
        }

        return  ResultEntity.error("删除品牌失败",500);
    }
}

