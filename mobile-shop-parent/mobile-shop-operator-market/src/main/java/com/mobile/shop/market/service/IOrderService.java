package com.mobile.shop.market.service;

import com.mobile.shop.market.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mobile.shop.market.entity.OrderDetailSku;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
public interface IOrderService extends IService<Order> {

    List<OrderDetailSku> selectDetail(Integer id);

    String selectAddress(Integer id);
}
