package com.mobile.shop.market.mapper;

import com.mobile.shop.market.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
public interface BrandMapper extends BaseMapper<Brand> {

}
