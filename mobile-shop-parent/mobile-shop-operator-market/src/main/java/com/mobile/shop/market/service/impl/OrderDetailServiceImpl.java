package com.mobile.shop.market.service.impl;

import com.mobile.shop.market.entity.OrderDetail;
import com.mobile.shop.market.mapper.OrderDetailMapper;
import com.mobile.shop.market.service.IOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {

}
