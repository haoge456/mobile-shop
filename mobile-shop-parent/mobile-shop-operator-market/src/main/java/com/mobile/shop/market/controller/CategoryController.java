package com.mobile.shop.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.market.entity.Brand;
import com.mobile.shop.market.entity.Category;
import com.mobile.shop.market.entity.Order;
import com.mobile.shop.market.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@RestController
@RequestMapping("/category")
public class CategoryController {


    @Autowired
    private ICategoryService iCategoryService;


    @RequestMapping("/list")
    public ResultEntity list(){


        return  ResultEntity.ok(iCategoryService.selectCateGoryByPage());
    }

    @RequestMapping("selectCateByPid")
    public ResultEntity selectCateByPid(){

        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();

        categoryQueryWrapper.eq("pid",0);
        return ResultEntity.ok(iCategoryService.list(categoryQueryWrapper));
    }


    @PostMapping("/insert")
    public ResultEntity insert(@RequestBody Category category
    ){

        boolean b = iCategoryService.save(category);

        if(b){
            return  ResultEntity.ok("添加样式成功",200);
        }

        return  ResultEntity.error("添加样式失败",500);
    }

    @PostMapping("/update")
    public ResultEntity update(@RequestBody Category category
    ){

        boolean b = iCategoryService.updateById(category);

        if(b){
            return  ResultEntity.ok("添加样式成功",200);
        }

        return  ResultEntity.error("添加样式失败",500);
    }

    @RequestMapping("/delete")
    public ResultEntity delete(Category category
    ){
        if(Integer.parseInt(category.getPid()) == 0){
             //如果是一级栏目 将会删除该栏目 并且删除以下所有的二级栏目
            QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
            categoryQueryWrapper.eq("pid",category.getId());
            //删除一级栏目下的所有二级栏目
            boolean b = iCategoryService.remove(categoryQueryWrapper);
        }
          //删除该栏目
        boolean b = iCategoryService.removeById(category.getId());
        if(b){
            return  ResultEntity.ok("删除样式成功",200);
        }

        return  ResultEntity.error("删除样式失败",500);
    }
}

