package com.mobile.shop.market.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SpecOptionList implements Serializable {


    private Integer id;


    private String name;
}
