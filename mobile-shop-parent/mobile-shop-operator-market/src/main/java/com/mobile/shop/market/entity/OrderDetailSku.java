package com.mobile.shop.market.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@Data
public class OrderDetailSku implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    private String name;

    private Integer num;

    private Double price;

    private String img;

    private String specname;

    private SpecOptionList specOptionList;

}
