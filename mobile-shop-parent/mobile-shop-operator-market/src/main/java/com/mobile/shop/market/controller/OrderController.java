package com.mobile.shop.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.City;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.market.entity.Brand;
import com.mobile.shop.market.entity.Order;
import com.mobile.shop.market.entity.OrderDetail;
import com.mobile.shop.market.entity.OrderDetailSku;
import com.mobile.shop.market.service.IBrandService;
import com.mobile.shop.market.service.IOrderDetailService;
import com.mobile.shop.market.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService iOrderService;

    @Autowired
    private IOrderDetailService iOrderDetailService;
    @RequestMapping("/list")
    public ResultEntity list(@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum, @RequestParam(value = "pageSize",defaultValue = "3")Integer pageSize,
                             Order order
    ){

        Page<Order> page = new Page<>(pageNum, pageSize);

        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        //对品牌商进行模糊查询
        if(order.getUid()!=null){
            wrapper.eq("uid",order.getUid());
        }

        if(order.getStatus()!=null){
            //区间查询
            wrapper.eq("status",order.getStatus());
        }
        if(order.getCreateTime1()!=null){
            wrapper.gt("create_time",order.getCreateTime1());
        }

        if(order.getCreateTime2()!=null){
            wrapper.le("create_time",order.getCreateTime2());
        }

        return  ResultEntity.ok(iOrderService.page(page,wrapper));
    }

    //查看订单详情

    @RequestMapping("/detail")
    public ResultEntity detail(Integer id
    ){


        //根据订单id查询该订单下的商品

        List<OrderDetailSku> list = iOrderService.selectDetail(id);

       if(list.size()>=0){
           return  ResultEntity.ok("查询订单商品成功",list);
       }

        return  ResultEntity.error("查询订单商品失败");
    }

    @RequestMapping("/address")
    public ResultEntity address(Integer id
    ){
        //根据订单id查询收货地址(详细地址)

        String address =  iOrderService.selectAddress(id);


        return  ResultEntity.ok("查询收货地址成功",address);
    }


    @RequestMapping("/delete")
    public ResultEntity delete(Integer id
    ){
        //删除订单表  删除订单详情表

        boolean b = iOrderService.removeById(id);

        QueryWrapper<OrderDetail> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("order_id",id);
        boolean b1 = iOrderDetailService.remove(objectQueryWrapper);

        if(b){
            //删除jyshop_order_detail表中关于order_id = id的数据
            return  ResultEntity.ok("删除订单成功",200);
        }

        return  ResultEntity.error("删除订单失败",500);
    }
}

