package com.mobile.shop.market.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.market.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
public interface CategoryMapper extends BaseMapper<Category> {

    /**
     * 查询所有的类目  根据pid进行查询
     * @param
     * @return
     */
    List<Category> selectCateGoryByPage();
}
