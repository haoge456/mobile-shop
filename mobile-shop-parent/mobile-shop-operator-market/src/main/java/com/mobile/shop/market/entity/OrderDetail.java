package com.mobile.shop.market.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("jy_order_detail")
public class OrderDetail implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = " id", type = IdType.AUTO)
    private Integer  id;

    /**
     * 商品id
     */
    private Integer skuId;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 小计
     */
    private BigDecimal totalPrice;


}
