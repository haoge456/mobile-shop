package com.mobile.shop.market.mapper;

import com.mobile.shop.market.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mobile.shop.market.entity.OrderDetailSku;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
public interface OrderMapper extends BaseMapper<Order> {

    List<OrderDetailSku> selectDetail(Integer id);

    String selectAddress(Integer id);
}
