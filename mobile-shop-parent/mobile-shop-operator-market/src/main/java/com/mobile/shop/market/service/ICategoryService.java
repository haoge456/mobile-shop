package com.mobile.shop.market.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.market.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
public interface ICategoryService extends IService<Category> {


    List<Category> selectCateGoryByPage();
}
