package com.mobile.shop.market.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class MyBatisPlusConfig {

    @Bean
    public PaginationInterceptor page(){

        return new PaginationInterceptor();
    }
}
