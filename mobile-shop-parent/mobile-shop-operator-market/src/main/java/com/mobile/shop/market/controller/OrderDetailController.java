package com.mobile.shop.market.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
@Controller
@RequestMapping("/order_detail")
public class OrderDetailController {

}

