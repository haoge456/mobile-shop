package com.mobile.shop.market.service;

import com.mobile.shop.market.entity.Brand;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-27
 */
public interface IBrandService extends IService<Brand> {

}
