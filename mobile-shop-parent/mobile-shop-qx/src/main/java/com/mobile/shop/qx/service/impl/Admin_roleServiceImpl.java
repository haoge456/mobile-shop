package com.mobile.shop.qx.service.impl;

import com.mobile.shop.qx.entity.Admin_role;
import com.mobile.shop.qx.mapper.Admin_roleMapper;
import com.mobile.shop.qx.service.IAdmin_roleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Service
public class Admin_roleServiceImpl extends ServiceImpl<Admin_roleMapper, Admin_role> implements IAdmin_roleService {

}
