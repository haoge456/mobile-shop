package com.mobile.shop.qx.mapper;

import com.mobile.shop.qx.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
public interface RoleMapper extends BaseMapper<Role> {

}
