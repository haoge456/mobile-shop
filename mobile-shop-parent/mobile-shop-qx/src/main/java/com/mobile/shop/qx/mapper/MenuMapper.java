package com.mobile.shop.qx.mapper;

import com.mobile.shop.qx.entity.Menu;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> findAll(String adminname);
}
