package com.mobile.shop.qx.service.impl;

import com.mobile.shop.qx.entity.Menu;
import com.mobile.shop.qx.mapper.MenuMapper;
import com.mobile.shop.qx.service.IMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Override
    public List<Menu> findAll(String adminname) {
        return baseMapper.findAll(adminname);
    }
}
