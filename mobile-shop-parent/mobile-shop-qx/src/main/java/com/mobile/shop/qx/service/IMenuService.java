package com.mobile.shop.qx.service;

import com.mobile.shop.qx.entity.Menu;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> findAll(String adminname);
}
