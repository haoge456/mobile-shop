package com.mobile.shop.qx.service.impl;

import com.mobile.shop.qx.entity.Role;
import com.mobile.shop.qx.mapper.RoleMapper;
import com.mobile.shop.qx.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
