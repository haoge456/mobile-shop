package com.mobile.shop.qx.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Data
@Accessors(chain = true)
@TableName("t_menu")
public class Menu extends Model<Menu> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 样式
     */
    private String icon;
    private String name;
    private String url;
    /**
     * 父id
     */
    private Integer p_id;
    private String path;
    private String component;
    private Date create_time;

    @TableField(exist = false)
    private List<Menu> children;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
