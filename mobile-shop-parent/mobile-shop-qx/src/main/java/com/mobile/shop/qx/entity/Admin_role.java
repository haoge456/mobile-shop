package com.mobile.shop.qx.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Data
@Accessors(chain = true)
@TableName("t_admin_role")
public class Admin_role extends Model<Admin_role> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer role_id;
    private Integer admin_id;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
