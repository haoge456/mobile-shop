package com.mobile.shop.qx.service;

import com.mobile.shop.qx.entity.Admin;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
public interface IAdminService extends IService<Admin> {

}
