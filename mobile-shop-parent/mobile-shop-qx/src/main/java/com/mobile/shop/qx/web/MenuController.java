package com.mobile.shop.qx.web;


import com.mobile.shop.qx.entity.Menu;
import com.mobile.shop.qx.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private IMenuService iMenuService;

    @RequestMapping("list")
    public List<Menu> list(String adminname){
        return iMenuService.findAll(adminname);
    }
}

