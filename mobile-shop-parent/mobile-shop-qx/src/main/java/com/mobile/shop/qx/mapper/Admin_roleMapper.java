package com.mobile.shop.qx.mapper;

import com.mobile.shop.qx.entity.Admin_role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
public interface Admin_roleMapper extends BaseMapper<Admin_role> {

}
