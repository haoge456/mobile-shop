package com.mobile.shop.qx.service.impl;

import com.mobile.shop.qx.entity.Admin;
import com.mobile.shop.qx.mapper.AdminMapper;
import com.mobile.shop.qx.service.IAdminService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

}
