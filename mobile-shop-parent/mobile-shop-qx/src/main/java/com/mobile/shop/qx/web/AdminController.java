package com.mobile.shop.qx.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

}

