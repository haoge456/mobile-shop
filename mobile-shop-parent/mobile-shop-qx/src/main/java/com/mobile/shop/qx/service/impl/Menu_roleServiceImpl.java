package com.mobile.shop.qx.service.impl;

import com.mobile.shop.qx.entity.Menu_role;
import com.mobile.shop.qx.mapper.Menu_roleMapper;
import com.mobile.shop.qx.service.IMenu_roleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Service
public class Menu_roleServiceImpl extends ServiceImpl<Menu_roleMapper, Menu_role> implements IMenu_roleService {

}
