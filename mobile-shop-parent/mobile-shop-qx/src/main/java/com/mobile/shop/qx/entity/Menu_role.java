package com.mobile.shop.qx.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Data
@Accessors(chain = true)
@TableName("t_menu_role")
public class Menu_role extends Model<Menu_role> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer menu_id;
    private Integer role_id;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
