package com.mobile.shop.statistics.service.impl;

import com.mobile.shop.common.entity.Tuser;
import com.mobile.shop.statistics.mapper.TuserMapper;
import com.mobile.shop.statistics.service.ITuserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class TuserServiceImpl extends ServiceImpl<TuserMapper, Tuser> implements ITuserService {

    @Autowired
    private TuserMapper tuserMapper;

    @Override
    public Integer selectCount1() {
        return tuserMapper.selectCount1();
    }
}
