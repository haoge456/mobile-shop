package com.mobile.shop.statistics.service.impl;

import com.mobile.shop.common.entity.Sku;
import com.mobile.shop.statistics.mapper.SkuMapper;
import com.mobile.shop.statistics.service.ISkuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements ISkuService {

    @Autowired
    private SkuMapper skuMapper;

    @Override
    public Integer selectCount1() {
        return skuMapper.selectCount1();
    }
}
