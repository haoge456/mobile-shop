package com.mobile.shop.statistics.service.impl;

import com.mobile.shop.common.entity.Spu;
import com.mobile.shop.statistics.mapper.SpuMapper;
import com.mobile.shop.statistics.service.ISpuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class SpuServiceImpl extends ServiceImpl<SpuMapper, Spu> implements ISpuService {

    @Autowired
    private SpuMapper spuMapper;

    @Override
    public Integer selectCount1() {
        return spuMapper.selectCount1();
    }
}
