package com.mobile.shop.statistics.service;

import com.baomidou.mybatisplus.service.IService;
import com.mobile.shop.common.entity.Order;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface IOrderService extends IService<Order> {

    Integer selectCount1();
}
