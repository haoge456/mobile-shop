package com.mobile.shop.statistics;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: MobileShopOperatorStatisticsApplication <br/>
 * Description: <br/>
 * date: 2020/6/1 19:48<br/>
 *
 * @author lenovo<br />
 * @version v1.0
 * @since JDK 1.8
 */
@SpringBootApplication
@MapperScan("com.mobile.shop.statistics.mapper")
public class MobileShopOperatorStatisticsApplication {
    public static void main(String[] args) {
        SpringApplication.run(MobileShopOperatorStatisticsApplication.class,args);
    }
}
