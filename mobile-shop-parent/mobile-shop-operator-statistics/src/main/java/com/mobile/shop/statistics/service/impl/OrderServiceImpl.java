package com.mobile.shop.statistics.service.impl;

import com.mobile.shop.common.entity.Order;
import com.mobile.shop.statistics.mapper.OrderMapper;
import com.mobile.shop.statistics.service.IOrderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Integer selectCount1() {
        return orderMapper.selectCount1();
    }
}
