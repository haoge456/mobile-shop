package com.mobile.shop.statistics.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mobile.shop.common.entity.Order;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface OrderMapper extends BaseMapper<Order> {

    Integer selectCount1();
}
