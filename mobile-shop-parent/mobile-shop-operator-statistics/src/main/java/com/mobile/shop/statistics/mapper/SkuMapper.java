package com.mobile.shop.statistics.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mobile.shop.common.entity.Sku;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface SkuMapper extends BaseMapper<Sku> {

    Integer selectCount1();
}
