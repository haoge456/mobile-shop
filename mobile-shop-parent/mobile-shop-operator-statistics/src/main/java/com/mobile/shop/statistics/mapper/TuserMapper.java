package com.mobile.shop.statistics.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mobile.shop.common.entity.Tuser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xin Long Wang
 * @since 2020-06-01
 */
public interface TuserMapper extends BaseMapper<Tuser> {

    Integer selectCount1();
}
