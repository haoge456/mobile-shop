package com.mobile.shop.statistics.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.common.entity.Tuser;
import com.mobile.shop.statistics.service.IOrderService;
import com.mobile.shop.statistics.service.ISkuService;
import com.mobile.shop.statistics.service.ISpuService;
import com.mobile.shop.statistics.service.ITuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: StatisticsController <br/>
 * Description: <br/>
 * date: 2020/6/1 19:57<br/>
 *
 * @author lenovo<br />
 * @version v1.0
 * @since JDK 1.8
 */
@RestController
@RequestMapping("statistics")
public class StatisticsController {

    @Autowired
    private IOrderService iOrderService;

    @Autowired
    private ISpuService iSpuService;

    @Autowired
    private ISkuService iSkuService;

    @Autowired
    private ITuserService iTuserService;


    /**
     * 查询总用户数
     * @return
     */
    @GetMapping(value = "getCountOfUser")
    public ResultEntity getCountOfUser(){
        Integer count = 0;
        //count = iTuserService.selectCount(new EntityWrapper<Tuser>());
        try {
            count = iTuserService.selectCount1();
        }catch (Exception e){
            return ResultEntity.error("500","查询失败",0);
        }
        return ResultEntity.ok("200","OK",count);
    }



    /**
     * 查询总订单数
     * @return
     */
    @GetMapping(value = "getCountOfOrder")
    public ResultEntity getCountOfOrder(){
        Integer count = 0;
        try {
            count = iOrderService.selectCount1();
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.error("500","查询失败",0);
        }
        return ResultEntity.ok("200","OK",count);
    }



    /**
     * 查询总货品
     * @return
     */
    @GetMapping(value = "getCountOfProduct")
    public ResultEntity getCountOfSpu(){
        Integer count = 0;
        try {
            count = iSpuService.selectCount1();
        }catch (Exception e){
            return ResultEntity.error("500","查询失败",0);
        }
        return ResultEntity.ok("200","OK",count);
    }


    /**
     * 查询总商品
     * @return
     */
    @GetMapping(value = "getCountOfGoods")
    public ResultEntity getCountOfSku(){
        Integer count = 0;
        try {
            count = iSkuService.selectCount1();
        }catch (Exception e){
            return ResultEntity.error("500","查询失败",0);
        }
        return ResultEntity.ok("200","OK",count);
    }
}
