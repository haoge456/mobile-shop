package com.mobile.shop.auth.service;

import com.baomidou.mybatisplus.service.IService;
import com.mobile.shop.auth.entity.Tuser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjq
 * @since 2020-05-21
 */
public interface ITuserService extends IService<Tuser> {

}
