package com.mobile.shop.auth.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@RestController
@RequestMapping("/admin")
public class AdminController {
    @GetMapping("/info")
    public Principal user(Principal member) {
        return member;
    }
}

