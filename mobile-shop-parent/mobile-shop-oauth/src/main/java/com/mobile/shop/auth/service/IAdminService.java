package com.mobile.shop.auth.service;

import com.baomidou.mybatisplus.service.IService;
import com.mobile.shop.auth.entity.Admin;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
public interface IAdminService extends IService<Admin> {

}
