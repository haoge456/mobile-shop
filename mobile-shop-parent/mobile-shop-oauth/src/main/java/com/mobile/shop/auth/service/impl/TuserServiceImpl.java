package com.mobile.shop.auth.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mobile.shop.auth.entity.Tuser;
import com.mobile.shop.auth.mapper.TuserMapper;
import com.mobile.shop.auth.service.ITuserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-21
 */
@Service
@Slf4j
public class TuserServiceImpl extends ServiceImpl<TuserMapper, Tuser> implements ITuserService, UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        log.info("用户进行认证的用户名为：{}",s);
        Tuser user = new Tuser();
        user.setUsername(s);
        Tuser user1 = this.baseMapper.selectOne(user);
        if (user1==null){
            throw  new UsernameNotFoundException("用户名不存在");
        }
        return new User(s,user1.getPassword(), AuthorityUtils.createAuthorityList("admin"));
    }
}
