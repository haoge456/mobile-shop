package com.mobile.shop.auth.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mobile.shop.auth.entity.Admin;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
