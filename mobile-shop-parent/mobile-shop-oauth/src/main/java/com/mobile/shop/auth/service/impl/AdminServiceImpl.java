package com.mobile.shop.auth.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mobile.shop.auth.entity.Admin;
import com.mobile.shop.auth.entity.Tuser;
import com.mobile.shop.auth.mapper.AdminMapper;
import com.mobile.shop.auth.service.IAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-31
 */
@Service
@Slf4j
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService, UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        log.info("用户进行认证的用户名为：{}",s);
        Admin admin = new Admin();
        admin.setAdminname(s);
        Admin admin1 = this.baseMapper.selectOne(admin);
        if (admin1==null){
            throw  new UsernameNotFoundException("用户名不存在");
        }
        return new User(s,admin1.getPassword(), AuthorityUtils.createAuthorityList("admin"));
    }
}
