package com.mobile.shop.auth.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mobile.shop.auth.entity.Tuser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjq
 * @since 2020-05-21
 */
public interface TuserMapper extends BaseMapper<Tuser> {

}
