package com.mobile.shop.common.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhao
 * @since 2020-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("jy_comment")
public class Comment implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    /**
     * 用户
     */
    private Integer uid;

    /**
     * 评论内容
     */
    private String content;

    private Integer spuId;


}
