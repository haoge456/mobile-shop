package com.mobile.shop.common.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhao
 * @since 2020-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("jy_sku_spec")
public class SkuSpec implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    /**
     * 具体的商品id
     */
    private Integer skuId;

    /**
     * 属性id
     */
    private Integer specId;

    /**
     * 属性值id
     */
    private Integer specOptionId;


}
