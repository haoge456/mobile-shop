package com.mobile.shop.common.entity;


import lombok.Data;



import java.io.Serializable;
import java.util.Date;

@Data
public class Log implements Serializable {
    private Integer id;

    /**
     * 操作管理员名称
     */
    private String adminName;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 操作类别
     */
    private String type;

    /**
     * 操作动作
     */
    private String active;

    /**
     * 操作状态
     */
    private String status;
    /**
     * 操作结果
     */
    private String result;

    /**
     * 备注信息
     */
    private String message;
}
