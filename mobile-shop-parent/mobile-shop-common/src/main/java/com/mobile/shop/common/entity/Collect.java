package com.mobile.shop.common.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhao
 * @since 2020-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("jy_collect")
public class Collect implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    /**
     * 收藏表里的用户id
     */
    private Integer uid;

    /**
     * 收藏表里的skuid
     */
    private Integer skuId;


}
