package com.mobile.shop.user.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.Address;
import com.mobile.shop.user.entity.AddressQuery;
import com.mobile.shop.user.entity.AddressVo;
import com.mobile.shop.user.mapper.AddressMapper;
import com.mobile.shop.user.service.IAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements IAddressService {

    @Autowired
    AddressMapper addressMapper;

    @Override
    public IPage<AddressVo> selectalladdress(Integer pageNum, Integer pageSize,AddressQuery addressQuery) {
        Page page = new Page(pageNum,pageSize);
        return addressMapper.selectalladdress(page,addressQuery);
    }

    @Override
    public boolean deleteAddress(String ids) {
        int flag = addressMapper.deleteall(ids);
        return flag>0;
    }
}
