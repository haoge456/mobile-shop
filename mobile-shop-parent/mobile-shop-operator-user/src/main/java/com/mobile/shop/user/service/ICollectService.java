package com.mobile.shop.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mobile.shop.common.entity.Collect;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mobile.shop.user.entity.CollectVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface ICollectService extends IService<Collect> {

    IPage<CollectVo> selectall(Integer pageNum, Integer pageSize,String nickname);
}
