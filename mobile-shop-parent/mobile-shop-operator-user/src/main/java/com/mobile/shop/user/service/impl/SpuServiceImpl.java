package com.mobile.shop.user.service.impl;

import com.mobile.shop.common.entity.Spu;
import com.mobile.shop.user.mapper.SpuMapper;
import com.mobile.shop.user.service.ISpuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@Service
public class SpuServiceImpl extends ServiceImpl<SpuMapper, Spu> implements ISpuService {

}
