package com.mobile.shop.user.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.Collect;
import com.mobile.shop.user.entity.CollectVo;
import com.mobile.shop.user.mapper.CollectMapper;
import com.mobile.shop.user.service.ICollectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@Service
public class CollectServiceImpl extends ServiceImpl<CollectMapper, Collect> implements ICollectService {

    @Autowired
    CollectMapper collectMapper;

    @Override
    public IPage<CollectVo> selectall(Integer pageNum, Integer pageSize,String nickname) {

        Page page = new Page(pageNum,pageSize);
        IPage<CollectVo> selectall = collectMapper.selectall(page, nickname);
        //selectall.getRecords().remove(0);

        return selectall;
    }
}
