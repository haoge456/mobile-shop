package com.mobile.shop.user.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.Address;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mobile.shop.user.entity.AddressQuery;
import com.mobile.shop.user.entity.AddressVo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface AddressMapper extends BaseMapper<Address> {
    IPage<AddressVo> selectalladdress(Page page, AddressQuery addressQuery);

    int deleteall(String ids);
}
