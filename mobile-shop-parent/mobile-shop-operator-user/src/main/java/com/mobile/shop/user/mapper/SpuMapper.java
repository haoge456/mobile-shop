package com.mobile.shop.user.mapper;

import com.mobile.shop.common.entity.Spu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface SpuMapper extends BaseMapper<Spu> {

}
