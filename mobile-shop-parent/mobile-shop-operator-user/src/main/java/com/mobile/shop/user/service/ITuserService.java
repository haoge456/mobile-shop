package com.mobile.shop.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mobile.shop.common.entity.Tuser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mobile.shop.user.entity.CollectQuery;
import com.mobile.shop.user.entity.TuserQuery;
import com.mobile.shop.user.entity.TuserVo;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface ITuserService extends IService<Tuser> {

    IPage<Tuser> listall(Integer pageNum, Integer pageSize, TuserQuery tuserQuery);

    boolean stopuser(String userid);

    boolean startuser(String userid);

    List<TuserVo> tusercollect(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, @RequestParam(defaultValue = "10",value = "pageSize")Integer pageSize, CollectQuery collectQuery);
}
