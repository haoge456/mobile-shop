package com.mobile.shop.user.mapper;

import com.mobile.shop.common.entity.Sku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mobile.shop.user.entity.SkuVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface SkuMapper extends BaseMapper<Sku> {
    List<SkuVo> selectbyid(Integer skuId);
}
