package com.mobile.shop.user.entity;

import lombok.Data;

@Data
public class AddressQuery {
    //收件人名字
    private String name;
    //手机
    private String phone;
    //省id
    private Integer sid;
    //市id
    private Integer cid;
    //县/区id
    private Integer xid;
}
