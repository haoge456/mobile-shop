package com.mobile.shop.user.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.user.entity.AddressQuery;
import com.mobile.shop.user.entity.TuserQuery;
import com.mobile.shop.user.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    IAddressService iAddressService;

    @RequestMapping("/selectAddress")
    public ResultEntity listall(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, @RequestParam(defaultValue = "10",value = "pageSize")Integer pageSize, AddressQuery addressQuery){
            return ResultEntity.ok(iAddressService.selectalladdress(pageNum,pageSize,addressQuery));
    }

    @PostMapping("/deleteAddress")
    public ResultEntity deleteAddress(String ids){
       return ResultEntity.ok(iAddressService.deleteAddress(ids));
    }
}

