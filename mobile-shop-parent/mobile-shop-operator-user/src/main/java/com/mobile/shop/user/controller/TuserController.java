package com.mobile.shop.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.Collect;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.common.entity.Tuser;

import com.mobile.shop.user.entity.CollectQuery;
import com.mobile.shop.user.entity.TuserQuery;

import com.mobile.shop.user.entity.TuserVo;
import com.mobile.shop.user.service.ITuserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@RestController
@RequestMapping("/tuser")
public class TuserController {

    @Autowired
    ITuserService iTuserService;

    @PostMapping("/list")
    public ResultEntity listall(Tuser tuser, @RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, @RequestParam(defaultValue = "10",value = "pageSize")Integer pageSize, TuserQuery tuserQuery){
        System.err.println("/list");
        IPage<Tuser> pageInfo = iTuserService.listall(pageNum,pageSize,tuserQuery);

        return ResultEntity.ok(pageInfo);
    }
    @PostMapping("/stopuser")
    public ResultEntity stopuser(String userid){
        return ResultEntity.ok(iTuserService.stopuser(userid));
    }
    @PostMapping("/startuser")
    public ResultEntity startuser(String userid){
        return ResultEntity.ok(iTuserService.startuser(userid));
    }
    @RequestMapping("/collect")
    public List<TuserVo> collectlist(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, @RequestParam(defaultValue = "10",value = "pageSize")Integer pageSize, CollectQuery collectQuery){
        return iTuserService.tusercollect(pageNum,pageSize,collectQuery);
    }


}

