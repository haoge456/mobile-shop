package com.mobile.shop.user.service.impl;

import com.mobile.shop.common.entity.Sku;
import com.mobile.shop.user.mapper.SkuMapper;
import com.mobile.shop.user.service.ISkuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements ISkuService {

}
