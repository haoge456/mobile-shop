package com.mobile.shop.user.service;

import com.mobile.shop.common.entity.Spu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface ISpuService extends IService<Spu> {

}
