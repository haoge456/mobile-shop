package com.mobile.shop.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.Address;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mobile.shop.user.entity.AddressQuery;
import com.mobile.shop.user.entity.AddressVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface IAddressService extends IService<Address> {
    IPage<AddressVo> selectalladdress(Integer pageNum, Integer pageSize,AddressQuery addressQuery);

    boolean deleteAddress(String ids);
}
