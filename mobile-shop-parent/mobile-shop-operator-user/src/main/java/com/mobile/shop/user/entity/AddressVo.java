package com.mobile.shop.user.entity;

import com.mobile.shop.common.entity.Address;
import lombok.Data;

@Data
public class AddressVo extends Address {
    //省名字
    private String sname;
    //市名字
    private String cname;
    //县名字
    private String xname;
    //用户名字
    private String uname;
}
