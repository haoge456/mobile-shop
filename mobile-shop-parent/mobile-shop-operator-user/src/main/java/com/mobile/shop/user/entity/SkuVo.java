package com.mobile.shop.user.entity;

import com.mobile.shop.common.entity.Sku;
import lombok.Data;

@Data
public class SkuVo extends Sku {
    private String spuname;
}
