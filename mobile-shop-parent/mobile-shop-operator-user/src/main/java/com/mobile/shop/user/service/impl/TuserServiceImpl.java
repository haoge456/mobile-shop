package com.mobile.shop.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.Tuser;
import com.mobile.shop.user.entity.CollectQuery;
import com.mobile.shop.user.entity.TuserQuery;
import com.mobile.shop.user.entity.TuserVo;
import com.mobile.shop.user.mapper.TuserMapper;
import com.mobile.shop.user.service.ITuserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@Service
public class TuserServiceImpl extends ServiceImpl<TuserMapper, Tuser> implements ITuserService {

    @Autowired
    TuserMapper tuserMapper;

    @Override
    public IPage<Tuser> listall(Integer pageNum, Integer pageSize, TuserQuery tuserQuery) {


        QueryWrapper queryWrapper = new QueryWrapper();
        if(tuserQuery.getNickname()!=null&&tuserQuery.getNickname()!=""){
            queryWrapper.like("nickname",tuserQuery.getNickname());
        }else if(tuserQuery.getSex()!=null&&tuserQuery.getSex()!=""){
            queryWrapper.eq("sex",tuserQuery.getSex());
        }else if(tuserQuery.getStarttime()!=null&&tuserQuery.getStarttime()!=""&&tuserQuery.getEndtime()!=null&&tuserQuery.getEndtime()!=""){
            queryWrapper.between("birthday",tuserQuery.getStarttime(),tuserQuery.getEndtime());
        }

        Page page= new Page(pageNum,pageSize);


        return tuserMapper.selectPage(page,queryWrapper);
    }

    @Override
    public boolean stopuser(String userid) {
        Tuser tuser = tuserMapper.selectById(userid);
        tuser.setStatus(2);
        int i = tuserMapper.updateById(tuser);
        return i>0;
    }

    @Override
    public boolean startuser(String userid) {
        Tuser tuser = tuserMapper.selectById(userid);
        tuser.setStatus(1);
        int i = tuserMapper.updateById(tuser);
        return i>0;
    }

    @Override
    public List<TuserVo> tusercollect(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, @RequestParam(defaultValue = "10",value = "pageSize")Integer pageSize, CollectQuery collectQuery) {
        return tuserMapper.tusercollect();
    }
}
