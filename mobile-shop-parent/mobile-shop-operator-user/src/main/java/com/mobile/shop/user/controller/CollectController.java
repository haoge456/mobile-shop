package com.mobile.shop.user.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.user.entity.CollectVo;
import com.mobile.shop.user.entity.SkuVo;
import com.mobile.shop.user.service.ICollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
@RestController
@RequestMapping("/operator/user/collect")
public class CollectController {
    @Autowired
    ICollectService iCollectService;

    @RequestMapping("/list")
    public ResultEntity listall(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, @RequestParam(defaultValue = "10",value = "pageSize")Integer pageSize){
        IPage<CollectVo> selectall = iCollectService.selectall(pageNum,pageSize,"123");

        System.err.println(selectall);
        return ResultEntity.ok(selectall);
    }

}

