package com.mobile.shop.user.entity;

import lombok.Data;

/**
 * user查询条件类
 */
@Data
public class TuserQuery {
    private String nickname;
    private String sex;
    private String starttime;
    private String endtime;
}
