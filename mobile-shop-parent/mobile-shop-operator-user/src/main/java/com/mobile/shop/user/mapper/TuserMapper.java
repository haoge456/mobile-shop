package com.mobile.shop.user.mapper;

import com.mobile.shop.common.entity.Tuser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mobile.shop.user.entity.TuserVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface TuserMapper extends BaseMapper<Tuser> {
    Tuser selectbyid(@Param("uid") Integer uid,@Param("nickname") String nickname);
    List<TuserVo> tusercollect();
}
