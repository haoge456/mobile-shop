package com.mobile.shop.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mobile.shop.common.entity.Collect;
import com.mobile.shop.common.entity.Tuser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhao
 * @since 2020-05-24
 */
@Data

public class CollectVo extends Collect {

    private Tuser tuser;
    private List<SkuVo> skulist;
}
