package com.mobile.shop.user.entity;

import lombok.Data;

@Data
public class CollectQuery {
    private String spuname;
    private String username;
    private String sex;
}
