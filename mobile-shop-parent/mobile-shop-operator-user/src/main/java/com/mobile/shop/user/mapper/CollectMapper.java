package com.mobile.shop.user.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mobile.shop.common.entity.Collect;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mobile.shop.user.entity.CollectVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2020-05-24
 */
public interface CollectMapper extends BaseMapper<Collect> {
    IPage<CollectVo> selectall(Page page,String nickname);
}
