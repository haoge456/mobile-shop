package com.mobile.shop.user.entity;

import com.mobile.shop.common.entity.Sku;
import com.mobile.shop.common.entity.Tuser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TuserVo extends Tuser {


    private List<SkuVo> skulist;
}
