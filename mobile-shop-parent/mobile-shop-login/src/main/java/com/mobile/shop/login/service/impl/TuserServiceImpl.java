package com.mobile.shop.login.service.impl;

import com.mobile.shop.login.entity.Tuser;
import com.mobile.shop.login.mapper.TuserMapper;
import com.mobile.shop.login.service.ITuserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjq
 * @since 2020-05-24
 */
@Service
public class TuserServiceImpl extends ServiceImpl<TuserMapper, Tuser> implements ITuserService {

}
