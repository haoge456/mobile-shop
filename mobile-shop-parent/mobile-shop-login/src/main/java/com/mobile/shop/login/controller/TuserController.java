package com.mobile.shop.login.controller;


import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.login.entity.Tuser;
import com.mobile.shop.login.service.ITuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wjq
 * @since 2020-05-24
 */
@RestController
@RequestMapping("/tuser")
public class TuserController {
    @Autowired
    private ITuserService iTuserService;

    /**
     * 用户注册
     * @param tuser
     * @return
     */
    @RequestMapping("register")
    public ResultEntity register(@RequestBody Tuser tuser){
        tuser.setStatus(1);
        boolean save = iTuserService.save(tuser);
        if(save){
            return ResultEntity.ok("200","注册成功",save);
        }
        return ResultEntity.error("500","注册失败",save);
    }
}

