package com.mobile.shop.login.service;

import com.mobile.shop.login.entity.Tuser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjq
 * @since 2020-05-24
 */
public interface ITuserService extends IService<Tuser> {

}
