package com.mobile.shop.login.mapper;

import com.mobile.shop.login.entity.Tuser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjq
 * @since 2020-05-24
 */
public interface TuserMapper extends BaseMapper<Tuser> {

}
