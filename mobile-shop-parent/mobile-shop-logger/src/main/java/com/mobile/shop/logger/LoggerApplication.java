package com.mobile.shop.logger;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class LoggerApplication {
    /**
     * 日志启动类
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(LoggerApplication.class,args);
    }
}
