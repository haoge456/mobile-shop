package com.mobile.shop.logger.controller;

import com.mobile.shop.common.entity.Log;
import com.mobile.shop.common.entity.ResultEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("logger")
public class LoggerController {
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 添加日志到mongodb
     * @param log
     * @return
     */
    @RequestMapping("log")
    public ResultEntity log(Log log){
        Log save = mongoTemplate.save(log);
        if(save!=null){
            return ResultEntity.ok("200","日志添加成功",save);
        }
        return ResultEntity.ok("500","日志添加失败",save);
    }
}
