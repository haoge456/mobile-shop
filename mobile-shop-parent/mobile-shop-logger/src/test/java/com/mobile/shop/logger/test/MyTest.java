package com.mobile.shop.logger.test;

import com.mobile.shop.common.entity.Log;
import com.mobile.shop.common.entity.ResultEntity;
import com.mobile.shop.logger.controller.LoggerController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class MyTest {

    @Autowired
    private LoggerController loggerController;

    /**
     * 日志打印测试
     */
    @Test
    public void save(){
        Log log = new Log();
        log.setId(2);
        log.setAdminName("admin");
        log.setCreateTime(new Date());
        log.setIp("192.168.125.124");
        ResultEntity log1 = loggerController.log(log);
        System.out.println(log1);
    }


}
