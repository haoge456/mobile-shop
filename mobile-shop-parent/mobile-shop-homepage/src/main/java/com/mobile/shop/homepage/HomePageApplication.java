package com.mobile.shop.homepage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import reactor.core.publisher.Mono;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan("com.mobile.shop")
public class HomePageApplication {


    public static void main(String[] args) {
        SpringApplication.run(HomePageApplication.class,args);
    }

}
