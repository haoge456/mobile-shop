package com.mobile.shop.homepage.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/homePage/")
public class HomePageController {


    //优惠劵的集合
    @RequestMapping("discountList")
    public Object discountList(){


           return  null;
      }

    //品牌商的集合
    @RequestMapping("brandList")
    public Object brandList(){


        return  null;
    }

    //首页的轮播图
    @RequestMapping("carouselList")
    public Object carouselList(){

        return  null;
    }

    //新品推荐 根据商品的上架时间倒序
    @RequestMapping("selectGoodsByCreateTime")
    public Object selectGoodsByCreateTime(){


        return  null;
    }

    //商品分类
    @RequestMapping("selectCategoryByPid")
    public Object selectCategoryByPid(){


        return  null;
    }
}
