package com.mobile.shop.mapper;

import com.mobile.shop.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author QHM
 * @since 2020-06-05
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
