package com.mobile.shop.service.impl;

import com.mobile.shop.entity.Comment;
import com.mobile.shop.mapper.CommentMapper;
import com.mobile.shop.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author QHM
 * @since 2020-06-05
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {

}
